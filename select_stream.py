#!/usr/bin/env python3
import sys
import re


def parse_nvr(somestring):
    somestring = somestring.strip()
    pattern = re.compile('^(.+)-(.+)$')
    match = pattern.search(somestring)
    if match:
        return match.group(1), match.group(2)
    return None


def compare_lists(list1, list2):
    i = 0
    while i < len(list1) and i < len(list2):
        part1 = list1[i]
        part2 = list2[i]

        if part1.isdigit():
            if part2.isdigit():
                part1 = int(part1)
                part2 = int(part2)
            else:
                return 1
        else:
            if part2.isdigit():
                return -1

        if part1 < part2:
            return -1
        elif part1 > part2:
            return 1
        i = i + 1

    if len(list1) > len(list2):
        return 1
    elif len(list1) < len(list2):
        return -1
    return 0


def compare_nvr(nvr1str, nvr2str):
    nvr1 = parse_nvr(nvr1str)
    nvr2 = parse_nvr(nvr2str)

    if nvr1 and nvr2:
        pass
    else:
        return None

    ver1, rel1 = nvr1
    ver2, rel2 = nvr2

    ver1 = ver1.split('.')
    ver2 = ver2.split('.')

    rel1 = rel1.split('.')
    rel2 = rel2.split('.')

    list1 = ver1 + rel1
    list2 = ver2 + rel2

    return compare_lists(list1, list2)


def nvr_in_range(nvr, nvr_low, nvr_high):
    if (compare_nvr(nvr_low, nvr) <= 0 and
            compare_nvr(nvr_high, nvr) >= 0):
        return True
    return False


mapping = {
    # For some streams like rhel7 we distinguish streams (e.g. z-streams) with
    # specific requirements.
    'rhel7': {
        'rhel70-z': ("3.10.0-123.1.1.el7", "3.10.0-123.999.999.el7"),
        'rhel71-z': ("3.10.0-229.1.1.el7", "3.10.0-229.999.999.el7"),
        'rhel72-z': ("3.10.0-327.1.1.el7", "3.10.0-327.999.999.el7"),
        'rhel73-z': ("3.10.0-514.1.1.el7", "3.10.0-514.999.999.el7"),
        'rhel74-z': ("3.10.0-693.1.1.el7", "3.10.0-693.999.999.el7"),
        'rhel75-z': ("3.10.0-862.1.1.el7", "3.10.0-862.999.999.el7"),
        'rhel76-z': ("3.10.0-957.1.1.el7", "3.10.0-957.999.999.el7"),
        'rhel7':    ("3.1-0.el7", "3.999-0.el7")
    },
    'rhel7-rt': {
        'rhel7-rt-z':  ("3.10.0-957.1.1.el7", "3.10.0-957.999.999.el7"),
        'rhel7-rt':    ("3.1-0.el7", "3.999-0.el7"),
    },
    'rhel8': {
        'rhel80-z': ("4.18.0-80.1.1.el8_0","4.18.0-80.999.999.el8_0"),
        'rhel8':    ("4.18.0-0.el8",       "4.18.0-999.el8"),
    },
    'rhel8-rt': {
        'rhel80-z-rt': ("4.18.0-80.1.1.el8_0","4.18.0-80.999.999.el8_0"),
        'rhel8-rt':    ("4.18.0-0.el8",       "4.18.0-999.el8"),
    }
}

if __name__ == '__main__':
    stream_type = sys.argv[1]
    nvr = sys.argv[2]

    # Strip kernel-/kernel-rt- prefix so the rest of the code is consistent
    if nvr.startswith('kernel-rt-'):
        nvr = nvr[len('kernel-rt-'):]
    elif nvr.startswith('kernel-'):
        nvr = nvr[len('kernel-'):]

    if stream_type in mapping.keys():
        # We have specified more precise mapping for rhels, so we'll use NVR
        # to map kernel to stream.
        nvr_found = False

        distinguished_stream = mapping[stream_type]
        for stream, nvr_range in distinguished_stream.items():
            nvr_low, nvr_high = nvr_range
            if nvr_in_range(nvr, nvr_low, nvr_high):
                print(stream)
                # mark: found
                nvr_found = True
                # take the first match, stop searching
                break
        if not nvr_found:
            # Raise exception here, so we can fixup stream mapping and restart
            # the pipeline.
            raise RuntimeError('stream for this nvr was not found')
    else:
        # pass-through
        print(stream_type)
